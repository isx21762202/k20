# K20 Kerberos server

isx21762202 2020-2021

 * **isx21762202/k20:server** Servidor kerberos detach. Crea els usuaris pere, pau, jordi, anna, marta/admin. Assignar-li el nom de host: *kserver.edt.org*

 * **isx21762202/k20:host** Host client de kerberos. Simplement amb eines kinit, klist i kdestory (no pam). El servidor al que contacta s'ha de dir *kserver.edt.org*.
