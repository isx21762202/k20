#!/bin/bash
# Kserver

cp /opt/docker/krb5.conf /etc/krb5.conf
cp /opt/docker/kadm5.acl /var/kerberos/krb5kdc/kadm5.acl
cp /opt/docker/kdc.conf /var/kerberos/krb5kdc/kdc.conf

kdb5_util create -s -P masterkey

for user in anna pere marta jordi pau user{01..10}
do
 kadmin.local -q "addprinc -pw k$user $user"
done

kadmin.local -q "addprinc -pw kmarta marta/admin"
kadmin.local -q "addprinc -pw ksuper super"

