#!/bin/bash
# Kserver

cp /opt/docker/krb5.conf /etc/krb5.conf
# dnf -y install
cp /opt/docker/pam_krb5.so /usr/lib64/security/pam_krb5.so

# Crear usuaris local01-local03 (IP + AP)
for user in local01 local02 local03
do
	useradd $user
	echo -e "$user\n$user\n" | passwd --stdin $user
done

# Crear usuaris kuser01.. el passwd està a kerberos
# for user in kuser01 kuser02 kuser03
# do
# 	useradd $user
# done
